const wasm = require("./hola/pkg/hola")

const fastify = require('fastify')({ logger: true })


fastify.post(`/`, async (request, reply) => {
  let jsonParameters = request.body

  return wasm.handle(jsonParameters)
})

const startService = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)
  } catch (error) {
    fastify.log.error(error)
  }
}
startService()

