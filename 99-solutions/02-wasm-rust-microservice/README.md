
## Create a rust function

```bash
cargo new --lib hola
```

Add this to `Cargo.toml`:


```toml
[lib]
name = "hola"
path = "src/lib.rs"
crate-type =["cdylib"]

[dependencies]
serde = { version = "1.0", features = ["derive"] }
wasm-bindgen = { version = "0.2", features = ["serde-serialize"] }
```

Change `./src/libs.rs`

```rust
use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize)]
pub struct Human {
    pub firstname: String,
    pub lastname: String,
}

#[derive(Serialize, Deserialize)]
pub struct Answer {
    pub text: String,
}

#[wasm_bindgen]
pub fn handle(value: &JsValue) -> JsValue {
    // deserialize value (parameter) to question
    let human: Human = value.into_serde().unwrap();

    // serialize answer to JsValue
    let answer = Answer {
        text: String::from(format!("Hola {} {}", human.firstname, human.lastname)),
    };

    return JsValue::from_serde(&answer).unwrap()
}

```

## Build

```bash
cd hola; wasm-pack build --target nodejs # 🖐🖐🖐
```

Then, the "distribution" files are located in `./hola/pkg`

## Test it

Create a new file `tests.js`

```javascript
const wasm = require("./hola/pkg/hola")

console.log(
  wasm.handle({
    firstname: "Bob",
    lastname: "Morane"
  })
)
```

```bash
node tests.js
```

### Query

Create a new file `index.js`

```javascript
const wasm = require("./hola/pkg/hola")

const fastify = require('fastify')({ logger: true })


fastify.post(`/`, async (request, reply) => {
  let jsonParameters = request.body

  return wasm.handle(jsonParameters)
})

const startService = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)
  } catch (error) {
    fastify.log.error(error)
  }
}
startService()
```


```bash
http POST http://0.0.0.0:8080/ firstname="Bob" lastname="Morane"
```

### Query from outside

```bash
# type the command below to get a "public" url
url=$(gp url 8080)
curl \
  -d '{"firstname":"Bill", "lastname":"Ballantine"}' \
  -H "Content-Type: application/json" \
  -X POST ${url}
```
