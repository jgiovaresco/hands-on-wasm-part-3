let fastifyServerOptions = {
  logger: true
}
const fastify = require('fastify')(fastifyServerOptions)

const fs = require('fs')
require("./wasm_exec")

async function runWasm(wasmFile, args) {
  const go = new Go()
  try {
    let { instance } = await WebAssembly.instantiate(wasmFile, go.importObject)
    if(args) go.argv = args
    go.run(instance) 
  } catch(err) {
    throw err
  }
}

const loadWasmFile = async () => {

  const wasmFile = fs.readFileSync('./hello/main.wasm')
  await runWasm(wasmFile)
  // new function(s) available  
}
loadWasmFile()

// Declare a route
fastify.post(`/`, async (request, reply) => {
  let jsonParameters = request.body

  return Handle(jsonParameters)
})

const startService = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)

  } catch (error) {
    fastify.log.error(error)
  }
}
startService()
