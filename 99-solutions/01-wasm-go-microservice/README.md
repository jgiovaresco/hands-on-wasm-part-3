# wasm go microservice

## Create a Go function

```bash
mkdir hello
cd hello
```

Add a new file `go.mod`

```text
module wasm-go-microservice

go 1.17
```

Add a new file `main.go`

```go
package main

import (
	"syscall/js"
)

func Handle(this js.Value, args []js.Value) interface{} {
	// get the first parameter
	human := args[0]
	// get members of human
	firstName := human.Get("firstname").String()
	lastName := human.Get("lastname").String()

	return map[string]interface{} {
		"text": "Hello " + firstName + " " + lastName,
	}
}

func main() {

	js.Global().Set("Handle",js.FuncOf(Handle))

	<-make(chan bool)
}
```

## Build the function

```bash
GOOS=js GOARCH=wasm go build -o main.wasm
```

## Test the function

Download `wasm_exec.js`

At the root ofx the project
```bash
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" .
```

Create a new file `tests.js`

```javascript
const fs = require('fs')
require("./wasm_exec")

async function runWasm(wasmFile, args) {
  const go = new Go()
  try {
    let { instance } = await WebAssembly.instantiate(wasmFile, go.importObject)
    if(args) go.argv = args
    go.run(instance) 
  } catch(err) {
    throw err
  }
}

const loadWasmFile = async () => {

  const wasmFile = fs.readFileSync('./hello/main.wasm')
  await runWasm(wasmFile)

  callfunction()
}
loadWasmFile()


const callfunction = () => {
  console.log(
    Handle({
      firstname: "Bob",
      lastname: "Morane"
    })
  )
}
```

```bash
node tests.js
```

## Serve the function

Create a new file `index.js`

```javascript
let fastifyServerOptions = {
  logger: true
}
const fastify = require('fastify')(fastifyServerOptions)

const fs = require('fs')
require("./wasm_exec")

async function runWasm(wasmFile, args) {
  const go = new Go()
  try {
    let { instance } = await WebAssembly.instantiate(wasmFile, go.importObject)
    if(args) go.argv = args
    go.run(instance) 
  } catch(err) {
    throw err
  }
}

const loadWasmFile = async () => {

  const wasmFile = fs.readFileSync('./hello/main.wasm')
  await runWasm(wasmFile)
  // new function(s) available  
}
loadWasmFile()

// Declare a route
fastify.post(`/`, async (request, reply) => {
  let jsonParameters = request.body

  return Handle(jsonParameters)
})

const startService = async () => {
  try {
    await fastify.listen(8080, "0.0.0.0")
    console.log("server listening on:", fastify.server.address().port)

  } catch (error) {
    fastify.log.error(error)
  }
}
startService()

```

```bash
node index.js
```


### Query

```bash
http POST http://0.0.0.0:8080/ firstname="Bob" lastname="Morane"
```

### Query from outside

```bash
# type the command below to get a "public" url
url=$(gp url 8080)
curl \
  -d '{"firstname":"Bill", "lastname":"Ballantine"}' \
  -H "Content-Type: application/json" \
  -X POST ${url}
```
