package main

import (
	"fmt"
)

func main() {
	fmt.Println("👋 Hello World 🌍 from Go")
	
	<-make(chan bool)
}
