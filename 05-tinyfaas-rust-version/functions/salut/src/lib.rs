use wasm_bindgen::prelude::*;
use serde::{Serialize, Deserialize};


#[derive(Serialize, Deserialize)]
pub struct Human {
    pub firstname: String,
    pub lastname: String,
}

#[derive(Serialize, Deserialize)]
pub struct Answer {
    pub text: String,
}

#[wasm_bindgen]
pub fn handle(value: &JsValue) -> JsValue {
    // deserialize value (parameter) to question
    let human: Human = value.into_serde().unwrap();

    // serialize answer to JsValue
    let answer = Answer {
        text: String::from(format!("Salut {} {}", human.firstname, human.lastname)),
    };

    return JsValue::from_serde(&answer).unwrap()
}
