#!/bin/bash

url_api="http://localhost:8080"
function_name="hola"
function_version="0.0.0"
data='{"firstname":"Bob", "lastname":"Morane"}'
curl -d "${data}" \
  -H "Content-Type: application/json" \
  -X POST "${url_api}/functions/call/${function_name}"

echo
echo

http POST "${url_api}/functions/call/${function_name}" \
firstname="Bob" \
lastname="Morane"